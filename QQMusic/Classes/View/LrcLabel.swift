//
//  LrcLabel.swift
//  QQMusic
//
//  Created by hjj on 2016/12/3.
//  Copyright © 2016年 hjjuny. All rights reserved.
//
import UIKit

class LrcLabel: UILabel {
    
    var progress : Double = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        UIColor.green.set()
        
        let drawRect = CGRect(x: 0, y: 0, width: rect.width * CGFloat(progress), height: rect.height)
        // /* R = S*Da */
        // S : 填充的透明度  --> 1.0
        // Da : 原有的透明度  --> 0.0/1.0
        // /* R = S*(1 - Da) */
        UIRectFillUsingBlendMode(drawRect, .sourceIn)
    }
}
